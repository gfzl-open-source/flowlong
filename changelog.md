
# 更新日志

## [v0.0.3] 2024.04.21

- 执行节点调整父任务节点记录携带优化
- 任务参与类型未知去除兼容不设置情况默认循序执行
- 新增测试加签节点存在多人时，当其中一人驳回异常问题
- 发起任务事件独立状态区分
- 修复会签节点驳回 Bug
- 其它代码优化


## [v0.0.2] 2024.04.18

- 新增节点条件参数处理器
- 处理器支持@Component注入
- 增加结束节点&测试
- 模型节点新增控制属性
- 新增会签支持加减签功能
- 新增状态区分任务驳回结束
- 优化缓存允许自定义注入流程模型解析器
- 优化允许注入json解析处理器
- 测试用例增加事务
- 增加 重新部署流程 的测试用例
- 优化初始赋值


## [v0.0.1] 2024.04.01

- 发布创始版